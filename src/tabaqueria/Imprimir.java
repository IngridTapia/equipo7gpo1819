
package tabaqueria;


import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;


public class Imprimir extends JFrame {
  public Imprimir(){
      marco();
  }
   public void marco(){
        JFrame f= new JFrame ();
        f.setSize(500,280);
        f.setLocation(100,100);
        f.setTitle("IMPRIMIR");
       // paneles que se van a utilizar en la forma
       
        JPanel panelMayor=new JPanel(new BorderLayout());
       
       JPanel panelNorte= new JPanel();
       JPanel panelCentro= new JPanel();
       JPanel panelSur= new JPanel();
       JPanel panelEste= new JPanel();
       JPanel panelOeste= new JPanel();
       
       //layout Managers a utilizar
             
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1 = new GridBagConstraints();
       panelCentro.setLayout(gbl1);
       
             
       GridLayout gl2=new GridLayout(1,6);
       panelSur.setLayout(gl2);
       
      JLabel lblTitulo=new JLabel("");
       
        //COMPONENTES para panel centro
       String[] lista1={"ENVIAR A ONENOTE 3","GUARDAR COMO PDF","MICROSOFT DOCUMENT WRITE","FAX","CANON","HP"};
       JLabel lbl1=new JLabel("\t\nIMPRESORA:");
       JComboBox cb1=new JComboBox(lista1);
       
       String[] lista2={"VERTICA","HORIZONTAl"};
       JLabel lbl2=new JLabel("\t\nDISEÑO:");
       JComboBox cb2=new JComboBox(lista2);
       
       String[] lista3={"BLANCO Y NEGRO","COLOR"};
       JLabel lbl3=new JLabel("\t\nCOLOR:");
       JComboBox cb3=new JComboBox(lista3);
      
       
       JLabel lbl4=new JLabel("\t\nHOJAS:");
       JRadioButton rb1 =new JRadioButton();
       rb1.setText("TODOS");
       
       JRadioButton rb2= new JRadioButton();
       rb2.setText("ALGUNOS");
       
       JLabel lbl5=new JLabel("DE:");
       JTextField tf5= new JTextField(2);
       JLabel lbl6=new JLabel("A");
       JTextField tf6= new JTextField(2);
       
           
       
       //componentes para panel sur
       JButton bs1=new JButton();
       JButton bs2=new JButton();
       JButton bs3=new JButton();
        JButton bs4=new JButton();
       
       bs1.setText("IMPRIMIR");
            
       bs2.setText("CANCELAR");
       
       bs3.setText("+ OPCIONES");
       bs4.setText("BUSCAR IMPRESORA");
       
       
       panelNorte.add(lblTitulo);
       
       gbc1.anchor=GridBagConstraints.WEST;
       
       panelCentro.add(lbl1,gbc1);
       panelCentro.add(cb1,gbc1);
       panelCentro.add(bs4,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=11;
       
       
       
       panelCentro.add(lbl2,gbc1);
       panelCentro.add(cb2,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=22;
       
        panelCentro.add(lbl3,gbc1);
        panelCentro.add(cb3,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=33;

        panelCentro.add(lbl4,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=44;
        panelCentro.add(rb1,gbc1);
          gbc1.gridwidth=1;
        gbc1.gridy=55;
       
        panelCentro.add(rb2,gbc1);
        //gbc1.gridwidth=1;
        gbc1.gridy=66;
       
       panelCentro.add(lbl5,gbc1);
       panelCentro.add(tf5,gbc1);
       gbc1.gridwidth=1;
        gbc1.gridy=77;
       panelCentro.add(lbl6,gbc1);
       panelCentro.add(tf6,gbc1);
      
       panelSur.add(bs1);
       panelSur.add(bs2);
       panelCentro.add(bs3,gbc1);
       gbc1.gridwidth=1;
        gbc1.gridy=88;
       
      
       //integrar paneles a paneles Mayor
       
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      
      //ASOCIAR EL PANEL MAYOR A LA FORMA O VENTANA
      f.add(panelMayor);
      
      //f.pack();
      
      f.setVisible(true);
      
       
     }
}