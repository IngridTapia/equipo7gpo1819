package tabaqueria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class BuscarProducto{
    public BuscarProducto(){
        marco();
}
     public void marco(){
        JFrame f= new JFrame ();
        f.setSize(450,200);
        f.setLocation(100,100);
        f.setTitle("BUSQUEDA DE PRODUCTOS");
       // paneles que se van a utilizar en la forma
       
        JPanel panelMayor=new JPanel(new BorderLayout());
       
       JPanel panelNorte= new JPanel();
       JPanel panelCentro= new JPanel();
       JPanel panelSur= new JPanel();
       JPanel panelEste= new JPanel();
       JPanel panelOeste= new JPanel();
       
       //layout Managers a utilizar
             
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1 = new GridBagConstraints();
       panelCentro.setLayout(gbl1);
       
             
       GridLayout gl2=new GridLayout(1,6);
       panelSur.setLayout(gl2);
       
      JLabel lblTitulo=new JLabel("INGRESA LOS DATOS PARA PODER BUSCAR EL PRODUCTO DESEADO");
       
        //COMPONENTES para panel centro
       String[] lista={"HOMBRE","MUJER","SODAS","DULCES","ROPA","HIGIENE INTIMA"};
       JLabel lbl5=new JLabel("\t\nLINEA:");
       JComboBox cb1=new JComboBox(lista);
       
       JLabel lbl1=new JLabel("\t\nCODIGO:");
       JTextField tf1= new JTextField(30);
       JLabel lbl2=new JLabel("\t\nNOMBRE:");
       JTextField tf2= new JTextField(30);
        

       //componentes para panel sur
       JButton bs1=new JButton();
       JButton bs2=new JButton();
       JButton bs3=new JButton();
       
       bs1.setText("INVENTARIO");
            
       bs2.setText("ACEPTAR");
       
       bs3.setText("CANCELAR");
       
       panelNorte.add(lblTitulo);
       
       gbc1.anchor=GridBagConstraints.WEST;
       
       panelCentro.add(lbl5,gbc1);
       panelCentro.add(cb1,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=11;
       
       panelCentro.add(lbl1,gbc1);
       panelCentro.add(tf1,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=22;
       
        panelCentro.add(lbl2,gbc1);
        panelCentro.add(tf2,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=33;
        
              
       
       
       panelSur.add(bs1);
       panelSur.add(bs2);
       panelSur.add(bs3);
       
      
       //integrar paneles a paneles Mayor
       
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      
      //ASOCIAR EL PANEL MAYOR A LA FORMA O VENTANA
      f.add(panelMayor);
      
      //f.pack();
      
      f.setVisible(true);
      
       
     }
}