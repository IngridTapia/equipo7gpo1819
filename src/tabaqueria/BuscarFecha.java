package tabaqueria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class BuscarFecha{
    public BuscarFecha(){
        marco();
}
     public void marco(){
        JFrame f= new JFrame ();
        f.setSize(500,150);
        f.setLocation(100,100);
        f.setTitle("BUSCAR POR FECHA");
       // paneles que se van a utilizar en la forma
       
        JPanel panelMayor=new JPanel(new BorderLayout());
       
       JPanel panelNorte= new JPanel();
       JPanel panelCentro= new JPanel();
       JPanel panelSur= new JPanel();
       JPanel panelEste= new JPanel();
       JPanel panelOeste= new JPanel();
       
       //layout Managers a utilizar
             
       GridBagLayout gbl1=new GridBagLayout();
       FlowLayout fl1=new FlowLayout(FlowLayout.CENTER);
       panelNorte.setLayout(fl1);
       
        GridLayout gbc1 = new  GridLayout();
       panelCentro.setLayout(gbl1);
       
       
       
             
       GridLayout gl2=new GridLayout(1,6);
       panelSur.setLayout(gl2);
       
      JLabel lblTitulo=new JLabel("PROPORCIONA LA FECHA EN QUE DESEAS SABER TU VENTA DIARIA");
            
        //COMPONENTES para panel centro
            
      String[] lista1={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
      "21","22","23","24","25","26","27","28","29","30","31"};
      JComboBox cb1=new JComboBox(lista1);
      
      String[] lista2={"ENERO","FEREBRO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
       JComboBox cb2=new JComboBox(lista2);
      
       String[] lista3={"1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008",
       "2009","2010","2011","2012","2013","2014","2015","2016"};
       JComboBox cb3=new JComboBox(lista3);
       
     
        

       //componentes para panel sur
       JButton bs1=new JButton();
       JButton bs2=new JButton();
       JButton bs3=new JButton();
             
      
       bs2.setText("BUSCAR");
       
       bs3.setText("SALIR");
       
       panelNorte.add(lblTitulo);
       
      
       
       panelCentro.add(cb1);
       panelCentro.add(cb2);
       panelCentro.add(cb3);
       panelSur.add(bs2);
       panelSur.add(bs3);
       
      
       //integrar paneles a paneles Mayor
       
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      
      //ASOCIAR EL PANEL MAYOR A LA FORMA O VENTANA
      f.add(panelMayor);
      
      //f.pack();
      
      f.setVisible(true);
      
       
     }
}