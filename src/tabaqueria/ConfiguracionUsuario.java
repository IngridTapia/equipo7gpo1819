
package tabaqueria;


import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;


public class ConfiguracionUsuario extends JFrame {
  public ConfiguracionUsuario(){
      marco();
  }
   public void marco(){
        JFrame f= new JFrame ();
        f.setSize(300,150);
        f.setLocation(100,100);
        f.setTitle("USUARIO");
       // paneles que se van a utilizar en la forma
       
        JPanel panelMayor=new JPanel(new BorderLayout());
       
       JPanel panelNorte= new JPanel();
       JPanel panelCentro= new JPanel();
       JPanel panelSur= new JPanel();
       JPanel panelEste= new JPanel();
       JPanel panelOeste= new JPanel();
       
       //layout Managers a utilizar
             
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1 = new GridBagConstraints();
       panelCentro.setLayout(gbl1);
       
             
       GridLayout gl2=new GridLayout(1,6);
       panelSur.setLayout(gl2);
       
      JLabel lblTitulo=new JLabel("");

       //componentes para panel sur
       JButton bs1=new JButton();
       JButton bs2=new JButton();
       JButton bs3=new JButton();
      
       
       bs1.setText("INICIAR USUARIO");
       
            
       bs2.setText("CREAR USUARIO");
       
       bs3.setText("SALIR");
             
       
       panelNorte.add(lblTitulo);
         
       panelCentro.add(bs1);
       panelCentro.add(bs2);
        panelSur.add(bs3);
           
       //integrar paneles a paneles Mayor
       
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      
      //ASOCIAR EL PANEL MAYOR A LA FORMA O VENTANA
      f.add(panelMayor);
      
      //f.pack();
      
      f.setVisible(true);
      
   }

}